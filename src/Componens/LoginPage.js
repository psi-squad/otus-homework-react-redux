import './css/Component.css';
import React, { useState } from 'react';
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import AuthActions from '../Actions/AuthActions'
import Header from './Header';

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from "react-bootstrap/Container";

export default function LoginForm(props) {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        const url = '/api/login';

        axios.post(url, {
            email: email, password: password
        })
            .then((resp) => {
                dispatch(AuthActions.logIn({ token: resp.data.token, status: 'Authorized' }))
            })
            .catch(function (error) {
                dispatch(AuthActions.logIn({ token: 'empty', status: 'Not authorized (' + error.response.data.error + ')' }))
            });
    };

    return (
        <div>
            <Header Title={'Login page'} />
            <Container className="p-2">
                <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={props.email} onChange={(e) => setEmail(e.target.value)} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={props.password} onChange={(e) => setPassword(e.target.value)} />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </Container>
        </div>
    );
}
