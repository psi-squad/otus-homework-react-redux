import './css/Component.css';
import React, { useState } from 'react';
import Header from './Header';


export default function HomePage() {

    return (
        <div>
            <Header Title={'Home page'} />
            To login successfully please use:<br/>
            <b>email: eve.holt@reqres.in</b><br />
            <b>password: cityslicka</b><br />
            on login page
        </div>
    );
}